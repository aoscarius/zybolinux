#!/bin/bash

# ZyBo Linux by A[O]scar(ius) Programmer
# This work is licensed under the Creative Commons (CC) 
# Attribution-Non Commercial-Share Alike 4.0 International License. 
# To view a copy of this license, visit 
# http://creativecommons.org/licenses/by-nc-sa/4.0/.
# For any questions or bug reports contact me at aoscarius+zybolinux(at)gmail.com

VER=0.29-rc

#### Customizable Parameters ####

CUSTOMIZEKERNEL="n" # Enable the customization GUI of the kernel source code
EXTRADYNMODULES="y" # Enable the compilation and installation of modules and source in the image 

FATSIZE=256 #MB		# Set the size of FAT32 (BOOT) partition
SDSIZE=4000 #MB     # Set the size of the full SD image. ROOT partition size is SDSIZE-FATSIZE

USEDIGILENTGIT="y"  # Enable the Digilent git repository for the download of the u-Boot and kernel source

VIVADOBASEDIR=/opt/Xilinx/Vivado/2015.4
FILESYSTEMURL=http://releases.linaro.org/15.06/ubuntu/vivid-images/developer/linaro-vivid-developer-20150618-705.tar.gz

#################################

# Check requirements
printf "Check requirements... "
if [ -z $(which git) ]; then
	echo "fail"
	echo "Script require git. Please install it before run."
	echo "On Debian based system: sudo apt-get install git"
	exit 1
fi

if [ -z $(which rsync) ]; then
	echo "fail"
	echo "Script require rsync. Please install it before run."
	echo "On Debian based system: sudo apt-get install rsync"
	exit 1
fi

if [ ! -z $1 ]; then
	VIVADOBASEDIR=$1
fi
if [ ! -d "$VIVADOBASEDIR" ]; then
	echo "fail"
	echo "Script require correct VIVADO installation path."
	echo "usage: ./zybolinux.sh <vivado_path>" 
	echo "or set VIVADOBASEDIR variable in the script."
	exit 1
fi

echo "done."

# Everything else needs to be run as root
if [ $(id -u) -ne 0 ]; then
    printf "Script must be run as root\n"
    exit 1
fi

SCRIPTNAME=`basename "$0"`
BNAME=${SCRIPTNAME%.*}

######## Logging Section ########
LOGDATE='date +%Y/%m/%d:%H:%M:%S'
LOGFILE=${BNAME}

exec &> >(tee -a $LOGFILE.log)
exec 2>&1
#################################

BASEDIRSCRIPT=$(dirname "$0")
BASEDIRPATH=${PWD}

if [[ "$BASEDIRPATH" != "${BASEDIRPATH/ /}" ]]; then
    echo "ERROR: Don't use a path with space in it to run this script."
    echo "       The script don't work if you do it."
    exit 1
fi

if [ -f "$BASEDIRPATH/$LOGFILE.log" ]; then
	rm $BASEDIRPATH/$LOGFILE.log
fi

BASEDIR=$BASEDIRPATH/${BNAME}_work
if [ ! -d "$BASEDIR" ]; then
	mkdir $BASEDIR
fi

if [ -d "$BASEDIR/dts" ]; then
	rm -rf $BASEDIR/dts
fi
mkdir $BASEDIR/dts

if [ -d "$BASEDIR/root_fs" ]; then
	rm -rf $BASEDIR/root_fs
fi
mkdir $BASEDIR/root_fs

if [ -d "$BASEDIR/BOOT_bin" ]; then
	rm -rf $BASEDIR/BOOT_bin
fi
mkdir $BASEDIR/BOOT_bin

chmod 777 $BASEDIR
chmod 777 $BASEDIR/dts
chmod 777 $BASEDIR/root_fs
chmod 777 $BASEDIR/BOOT_bin

cat << _EOF_
================================================================================
  ███████╗██╗   ██╗██████╗  ██████╗     ██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗
  ╚══███╔╝╚██╗ ██╔╝██╔══██╗██╔═══██╗    ██║     ██║████╗  ██║██║   ██║╚██╗██╔╝
    ███╔╝  ╚████╔╝ ██████╔╝██║   ██║    ██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝ 
   ███╔╝    ╚██╔╝  ██╔══██╗██║   ██║    ██║     ██║██║╚██╗██║██║   ██║ ██╔██╗ 
  ███████╗   ██║   ██████╔╝╚██████╔╝    ███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗
  ╚══════╝   ╚═╝   ╚═════╝  ╚═════╝     ╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝
               Linux Embedded Image Generator Script v. $VER	
                        by A[O]scar(ius) Programmer
================================================================================

_EOF_

TSTARTTIME=$(date +%s)
echo "Start script process at "`$LOGDATE`"..."
echo "BASEDIR: $BASEDIR"

cat << _EOF_
 _____ _                  ___   
|   __| |_ ___ ___ ___   |_  |  
|__   |  _| .'| . | -_|   _| |_ 
|_____|_| |__,|_  |___|  |_____|
              |___|             	   
      Preparing Enviroment
      
_EOF_

if [ "$USEDIGILENTGIT" = "y" -o "$USEDIGILENTGIT" = "Y" ]; then
	GITLINUX=https://github.com/Digilent/linux-Digilent-Dev
	GITUBOOT=https://github.com/Digilent/u-boot-Digilent-Dev
else
	GITLINUX=https://github.com/Xilinx/linux-xlnx
	GITUBOOT=https://github.com/Xilinx/u-boot-xlnx
fi

if [ ! -d "$BASEDIR/linux-zybo" ]; then
	echo "Cloning linux kernel..."
	git clone $GITLINUX $BASEDIR/linux-zybo
	rm -rf .git
	echo
fi

if [ ! -d "$BASEDIR/u-boot-zybo" ]; then
	echo "Cloning u-boot..."
	git clone $GITUBOOT $BASEDIR/u-boot-zybo
	rm -rf .git
	echo
fi

if [ ! -d "$BASEDIR/device-tree-zybo" ]; then
	echo "Cloning Xilinx DTS Generator SDK repository"
	git clone https://github.com/Xilinx/device-tree-xlnx $BASEDIR/device-tree-zybo
	rm -rf .git
	echo
fi

# Set enviroment
export ARCH=arm
export CROSS_COMPILE=arm-xilinx-linux-gnueabi-
source $VIVADOBASEDIR/settings64.sh
export PATH=$PATH:$BASEDIR/u-boot-zybo/tools:$BASEDIR/linux-zybo/scripts/dtc

cat << _EOF_
 _____ _                  ___ 
|   __| |_ ___ ___ ___   |_  |
|__   |  _| .'| . | -_|  |  _|
|_____|_| |__,|_  |___|  |___|
              |___|           
 Compile the u-Boot and Kernel
   
_EOF_

# Compiling linux kernel
echo "Compiling linux kernel..."
cd $BASEDIR/linux-zybo
make xilinx_zynq_defconfig
if [ "$CUSTOMIZEKERNEL" = "y" -o "$CUSTOMIZEKERNEL" = "Y" ]; then
	make menuconfig
fi
# Patching the kernel to enable UIO
sed -i 's/# CONFIG_UIO is not set/CONFIG_UIO=y/' $BASEDIR/linux-zybo/.config &>/dev/null 						
sed -i 's/# CONFIG_UIO_PDRV_GENIRQ is not set/CONFIG_UIO_PDRV_GENIRQ=y/' $BASEDIR/linux-zybo/.config &>/dev/null 
sed -i 's/# CONFIG_UIO_DMEM_GENIRQ is not set/CONFIG_UIO_DMEM_GENIRQ=y/' $BASEDIR/linux-zybo/.config &>/dev/null
sed -i 'N;s/static struct of_device_id uio_of_genirq_match\[\].*=.*{.*{ \/\* This is filled with module_parm \*\/ },/static struct of_device_id uio_of_genirq_match\[\] = {\n\t{ .compatible = "generic-uio", },\n\t{ \/\* This is filled with module_parm \*\/ },/g' $BASEDIR/linux-zybo/drivers/uio/uio_pdrv_genirq.c &>/dev/null
# Start make
make
if [ "$EXTRADYNMODULES" = "y" -o "$EXTRADYNMODULES" = "Y" ]; then
	make modules
fi
echo

# Paching and compiling u-Boot
echo "Patching and compiling u-Boot..."
sed -i 'N;s/"[fat]*load mmc 0 .* ${ramdisk_image}.*"bootm \(.*\) \(.*\) \(.*\); "/"bootm \1 - \3; "/g' $BASEDIR/u-boot-zybo/include/configs/zynq-common.h &>/dev/null 
sed -i 'N;s/"[fat]*load mmc 0 .* ${ramdisk_image}.*"bootm \(.*\) \(.*\) \(.*\); "/"bootm \1 - \3; "/g' $BASEDIR/u-boot-zybo/include/configs/zynq_zybo.h	&>/dev/null 	#This for patch u-boot digilent version
sed -i 'N;s/"[fat]*load mmc 0 .* ${ramdisk_image}.*"bootm \(.*\) \(.*\) \(.*\); "/"bootm \1 - \3; "/g' $BASEDIR/u-boot-zybo/include/configs/zynq_common.h &>/dev/null  #This for patch u-boot digilent version
cd $BASEDIR/u-boot-zybo
make zynq_zybo_config
make
echo

cat << _EOF_
 _____ _                  ___ 
|   __| |_ ___ ___ ___   |_  |
|__   |  _| .'| . | -_|  |_  |
|_____|_| |__,|_  |___|  |___|
              |___|           
    Compile all boots file
 
_EOF_

# Make uImage
cd $BASEDIR/linux-zybo
echo "Make uImage"
# Compile and add install kernel extra modules
if [ "$EXTRADYNMODULES" = "y" -o "$EXTRADYNMODULES" = "Y" ]; then
	make modules
	make UIMAGE_LOADADDR=0x00008000 uImage modules
else
	make UIMAGE_LOADADDR=0x00008000 uImage
fi
echo

# Build Device Tree Blob
echo "Build Device Tree Blob"

cat << _EOF_

================================ Alert Messagge ================================

Before you continue you need to create a customized Device Tree Structure to allow access to any custom peripherals. 

To do it follow this steps:

Add the BSP repository in SDK (for SDK 2014.2 and later select "device-tree-zybo" from the checked out git area):

	* SDK Menu: Xilinx Tools > Repositories > New... (<bsp repo>) > OK
	
Create a Device Tree Board Support Package (BSP):

	* SDK Menu: File > New > Board Support Package > Board Support Package OS: device-tree > Finish
	
Don't modify anything and press Finish and after Ok. Now copy all *.dts generated files in ${BNAME}_work/dts folder.

================================================================================

_EOF_
read -r -p "Read carefully this message and when you are ready press a key to continue ..." -n1 key
echo

# If not manual created DTS copy default DTS
cd $BASEDIR/linux-zybo
if [ ! -f "$BASEDIR/dts/system.dts" ]; then
	echo "ERROR: no system.sts file found. The devicetree.dtb not created."
	read -r -p "Press a key if you want proceed with manual creation of it or Q if you want quit and re-run the script" -n1 key
	if [ "$key" = "q" -o "$key" = "Q" ]; then
		exit 1
	fi
else

	# Patching DTS
	echo "Patching and compile DTB"
	sed -i 's/bootargs[A-Za-z0-9",. =\/]*/bootargs = "console=ttyPS0,115200n8 root=\/dev\/mmcblk0p2 rw earlyprintk rootfstype=ext4 rootwait devtmpfs.mount=1"/' $BASEDIR/dts/system.dts

cat << _EOF_ > $BASEDIR/dts/dtspatch.dts
/* ZyBo Linux (BY) A[O]scar(ius) Programmer
 * Patch the Device Tree Source of the ZyBo Board
 */

/include/ "system.dts"
/ {
    usb_phy0: phy0 {
        #phy-cells = <0>;
        compatible = "usb-nop-xceiv";
        reset-gpios = <&gpio0 46 1>;
    };
};
&gem0 {
    local-mac-address = [00 0a 35 00 00 00];
    phy-mode = "rgmii-id";
    status = "okay";
    phy-handle = <&phy0>;
    xlnx,eth-mode = <0x1>;
    xlnx,ptp-enet-clock = <0x6750918>;
    #address-cells = <1>;
    #size-cells = <0>;
    phy0: phy@1 {
        compatible = "realtek,RTL8211E";
        device_type = "ethernet-phy";
        reg = <1>;
    };
};
&usb0 {
    status = "okay";
    dr_mode = "host";
    usb-phy = <&usb_phy0>;
};
_EOF_

	dtc -I dts -O dtb -o $BASEDIR/dts/devicetree.dtb $BASEDIR/dts/dtspatch.dts

cat << _EOF_ > $BASEDIR/dts/recompile
#!/bin/bash

# Launch this script to recompile the Device Tree Blob
./dtc -I dts -O dtb -o devicetree.dtb ethernet.dts
_EOF_

	cp $BASEDIR/linux-zybo/scripts/dtc/dtc $BASEDIR/dts/dtc
	chmod 777  $BASEDIR/dts/dtc
	chmod 777  $BASEDIR/dts/recompile

fi


# Making BOOT.bin
cat << _EOF_

================================ Alert Messagge ================================

Before you continue you need to copy the FSBL.elf and <your_bistream>.bit (you need rename it in 'system.bit') in the ${BNAME}_work/BOOT_bin folder.

To do it follow this steps:

Create a new Application Project:

	* SDK Menu: File > New > Application Project
	
Call it FSBL and in the Next screen select Zynq FSBL template. Modify (if you want)  and compile it. Next put FSBL.elf, from Debug folder, in the correct place.

================================================================================

_EOF_
read -r -p "Read carefully this message and when you are ready press a key to continue ..." -n1 key
echo

# Create output.big for boot generator
if [ ! -f "${BASEDIR}/BOOT_bin/system.bit" -o ! -f "${BASEDIR}/BOOT_bin/FSBL.elf" ]; then
	echo "ERROR: no system.bit or FSBL.elf file found. The BOOT.bin not created."
	read -r -p "Press a key if you want proceed with manual creation of it or Q if you want quit and re-run the script" -n1 key
	if [ "$key" = "q" -o "$key" = "Q" ]; then
		exit 1
	fi
else

cat << _EOF_ > $BASEDIR/BOOT_bin/output.bif
the_ROM_image:
{
	[bootloader]${BASEDIR}/BOOT_bin/FSBL.elf
	${BASEDIR}/BOOT_bin/system.bit
	${BASEDIR}/BOOT_bin/u-boot.elf
}
_EOF_

	cp $BASEDIR/u-boot-zybo/u-boot $BASEDIR/BOOT_bin/u-boot.elf
	bootgen -image $BASEDIR/BOOT_bin/output.bif -o $BASEDIR/BOOT_bin/BOOT.bin -w on
	chmod 777 $BASEDIR/BOOT_bin/BOOT.bin
	
fi

cat << _EOF_
 _____ _                  ___ 
|   __| |_ ___ ___ ___   | | |
|__   |  _| .'| . | -_|  |_  |
|_____|_| |__,|_  |___|    |_|
              |___|           
     Creating SD Image
      
_EOF_

##### SD Creation #####
cd $BASEDIR
SDBASEDIR=sd_image

# Create the disk and partition it
echo "Creating image file for ZyboSD"
if [ ! -f "$BASEDIR/${BNAME}.img" ]; then
	# NOTE: Size of dd is in MiB
	dd if=/dev/zero of=$BASEDIR/${BNAME}.img bs=953674 count=$SDSIZE 
		
	chmod 777 $BASEDIR/${BNAME}.img
	parted $BASEDIR/${BNAME}.img --script -- mklabel msdos
	parted $BASEDIR/${BNAME}.img --script -- mkpart primary fat32 4 $FATSIZE
	parted $BASEDIR/${BNAME}.img --script -- mkpart primary ext4 $FATSIZE -1
fi

# Set the partition variables
loopdevice=`losetup -f --show $BASEDIR/${BNAME}.img`
device=`kpartx -va $loopdevice| sed -E 's/.*(loop[0-9])p.*/\1/g' | head -1`
device="/dev/mapper/${device}"
bootp=${device}p1
rootp=${device}p2

# Create file systems
mkfs.vfat -n BOOT -F 32 $bootp
mkfs.ext4 -L ROOT $rootp

# Create the dirs for the partitions and mount them
if [ -d "$BASEDIR/$SDBASEDIR" ]; then
	rm -rf $BASEDIR/$SDBASEDIR
fi
mkdir $BASEDIR/$SDBASEDIR
mkdir -p $BASEDIR/$SDBASEDIR/boot $BASEDIR/$SDBASEDIR/root
mount $bootp $BASEDIR/$SDBASEDIR/boot
mount $rootp $BASEDIR/$SDBASEDIR/root

echo "Coping boot files"
cp $BASEDIR/BOOT_bin/BOOT.bin $BASEDIR/$SDBASEDIR/boot/BOOT.bin	2>/dev/null
cp $BASEDIR/linux-zybo/arch/arm/boot/uImage $BASEDIR/$SDBASEDIR/boot/uImage
cp $BASEDIR/dts/devicetree.dtb $BASEDIR/$SDBASEDIR/boot/devicetree.dtb 2>/dev/null

# Get root file system
if [ ! -f "$BASEDIR/RFS.ark" ]; then
	echo "Download the files ystem..."
	wget $FILESYSTEMURL -O $BASEDIR/RFS.ark
fi

chmod 777 "$BASEDIR/RFS.ark"
tar -C $BASEDIR/root_fs -xzpf $BASEDIR/RFS.ark
# Coping fyle system
echo "Rsync rootfs into image file"
rsync -aHv -q $BASEDIR/root_fs/binary/ $BASEDIR/$SDBASEDIR/root

# Add automounting fat partition
echo "Tweaks the file system..."
mkdir $BASEDIR/$SDBASEDIR/root/uboot
echo "/dev/mmcblk0p1  /uboot          vfat    defaults          0       0" >> $BASEDIR/$SDBASEDIR/root/etc/fstab
echo "/dev/mmcblk0p2  /               ext4    defaults,noatime  0       0" >> $BASEDIR/$SDBASEDIR/root/etc/fstab

# Add config for the ethernet device
echo "Enable the ethernet device by default"
cat << _EOF_ > $BASEDIR/$SDBASEDIR/root/etc/network/interfaces
auto eth0
allow-hotplug eth0
iface eth0 inet dhcp
_EOF_

# Compile and add install kernel extra modules and headers
if [ "$EXTRADYNMODULES" = "y" -o "$EXTRADYNMODULES" = "Y" ]; then
	cd $BASEDIR/linux-zybo
	echo "Install kernel source and headers"
	make INSTALL_HDR_PATH=$BASEDIR/$SDBASEDIR/root/usr headers_install
	make INSTALL_MOD_PATH=$BASEDIR/$SDBASEDIR/root modules_install
	
	# Copy the source of kernel for compilation of modules, etc
	# Exclude all file produced from compilation of the kernel

	echo "Rsync kernel source into image file"
	mkdir $BASEDIR/$SDBASEDIR/root/usr/src/$(cat $BASEDIR/linux-zybo/include/config/kernel.release)
	rsync -aHv -q $BASEDIR/linux-zybo/ $BASEDIR/$SDBASEDIR/root/usr/src/$(cat $BASEDIR/linux-zybo/include/config/kernel.release)

	# Patch the correct symbolic link file
	rm -f $BASEDIR/$SDBASEDIR/root/lib/modules/$(cat $BASEDIR/linux-zybo/include/config/kernel.release)/build
	rm -f $BASEDIR/$SDBASEDIR/root/lib/modules/$(cat $BASEDIR/linux-zybo/include/config/kernel.release)/source
	ln -s /usr/src/$(cat $BASEDIR/linux-zybo/include/config/kernel.release) $BASEDIR/$SDBASEDIR/root/lib/modules/$(cat $BASEDIR/linux-zybo/include/config/kernel.release)/build
	ln -s /usr/src/$(cat $BASEDIR/linux-zybo/include/config/kernel.release) $BASEDIR/$SDBASEDIR/root/lib/modules/$(cat $BASEDIR/linux-zybo/include/config/kernel.release)/source
	
	cd $BASEDIR/$SDBASEDIR/root/usr/src/$(cat $BASEDIR/linux-zybo/include/config/kernel.release)
	make clean

	cd $BASEDIR
fi

# Add a funny customization
cat << _EOF_ >> $BASEDIR/$SDBASEDIR/root/root/.bashrc

	ntpdate ntp.ubuntu.com > /dev/null
	echo
	echo -e "Hi! You are on \e[4;91m\$(uname -sr)\e[0m based system created by \e[0;32mZyBo Linux Script\e[0m"
	echo
_EOF_

# Unmount partitions
umount $bootp
umount $rootp
rm -rf $BASEDIR/$SDBASEDIR

kpartx -dv $loopdevice
losetup -d $loopdevice

TENDTIME=$(date +%s)
echo "End script process at "`$LOGDATE`" in $(expr $TENDTIME - $TSTARTTIME) seconds."

cat << _EOF_

Now you can install your image on an SDHC card with

	sudo umount /dev/sdx*
	sudo dd bs=4M if=${BNAME}_work/${BNAME}.img of=/dev/sdx

where sdx is the name of your card. Thanks for use this script and goodbye...

_EOF_

if [ ! -f "$BASEDIR/BOOT_bin/BOOT.bin" -o ! -f "$BASEDIR/dts/devicetree.dtb" ]; then
	echo "WARNING: The image is not boatable. BOOT.bin and devicetree.dtb not created automatically."
	echo "Proceed to manual creation and copy of this file or re-run the script."
fi

