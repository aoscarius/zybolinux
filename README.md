# ZyBo Linux SD Generator v0.29-rc

### Introduction
This script was written to simplify the process of creating a GNU / Linux distribution specifically for the board ZyBo of the Digilent.

The script, automatically:

1. Download all the necessary sources for the compilation of the second stage boot loader U-Boot and the Linux kernel
2. Prepare the development environment and apply the necessary patches to the proper functioning of the system from an SD Card.
3. Configure and compile all in an appropriate manner.
4. Compile the Device Tree Blob with the included fix for the ethernet enable and USB enable.
5. Generate BOOT.bin
6. Create and populate a full SD Card image ready to use, with the file system Linaro, to write directly on a physical medium.
7. Apply a few tweaks to the file system and, if enabled, copy the kernel sources and headers to allow the compilation of the kernel modules directly from the ZyBo.

The script does not require any intervention, except for the steps 4 and 5 necessarily external (in which guide you through appropriate instructions).
It's necessary, in fact, provide to the script (in the appropriate folders) the FSBL already compiled (FSBL.elf), your generated Bitstream (*.bit), and the Device Tree Source (*.dts). All these files are to be generated from the Vivado SDK and the script gives details on how to proceed and will provide what is necessary.

### Requirements
The script require that you have installed 

- git
- rsync
- Vivado 201x.x (also WebPack)

on your machine. 

### Installation
Once downloaded, apply the following commands to run it:

```bash
$ chmod +x zybolinux.sh
$ sudo ./zybolinux.sh
```

The script must be run with admin permissions (to be able to perform various automatic mounts for the generation and population of the SD Card image).

### Configurations
It's possible to make a few small customization to the script by changing internal dedicated variables and in particular:
```bash
#### Customizable Parameters ####
CUSTOMIZEKERNEL="n" # Enable the customization GUI of the kernel source code
```
Enable the graphical kernel customization gui, to permit the customization of the kernel settings.
```bash
EXTRADYNMODULES="y" # Enable the compilation and installation of modules and source in the image 
```
Enable the compilation and installation of extra modules and the copy of linux source on the SD image, to compile the kernel modules direct on the board at runtime.
```bash
FATSIZE=256 #MB		# Set the size of FAT32 (BOOT) partition
SDSIZE=4000 #MB     # Set the size of the full SD image. ROOT partition size is SDSIZE-FATSIZE
```
Config the size of FAT32 partition (which contains the boot files) and the entire SD image.
```bash
USEDIGILENTGIT="y"  # Enable the Digilent git repository for the download of the u-Boot and kernel source
```
Select the official Digilent git repository for the download of u-Boot and kernel source.
```bash
VIVADOBASEDIR=/opt/Xilinx/Vivado/2015.4
FILESYSTEMURL=http://releases.linaro.org/15.06/ubuntu/vivid-images/developer/linaro-vivid-developer-20150618-705.tar.gz
#################################
```
Set the correct installation path of Vivado (if it's not the same to be corrected or passed as a parameter to the script) and the url of the file system to use.

Currently the script uses the latest developer version of the file system Linaro (Ubuntu based) and supports only file system Linaro (has not been tested with other).

### Todos
- Implement patch for ethernet enable [DONE]
- Add the possibility to have a complete development environment on board [DONE]
- Implement auto resize of image at runtime

### Contact
**Author**: [A[O]scar(ius)](https://gitlab.com/u/aoscarius)

**Git Repository**:  [https://gitlab.com/aoscarius/zybolinux](https://gitlab.com/aoscarius/zybolinux)
   
License
----
ZyBo Linux by [A[O]scar(ius)](https://gitlab.com/u/aoscarius) is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
